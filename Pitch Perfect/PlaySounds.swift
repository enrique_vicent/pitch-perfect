//
//  PlaySounds.swift
//  Pitch Perfect
//
//  Created by Enrique Vicent on 18/2/16.
//  Copyright © 2016 Enrique Vicent. All rights reserved.
//

import UIKit
import AVFoundation

class PlaySounds: UIViewController {

    @IBOutlet weak var SlowButton: UIButton!
    @IBOutlet weak var FastButton: UIButton!
    @IBOutlet weak var StopButton: UIButton!

    var audioPlayer = AVAudioPlayer()
    var audioSession = AVAudioSession.sharedInstance()
    var audioEngine:AVAudioEngine!
    var receivedAudio: RecordedAudio!
    var audioFile: AVAudioFile!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nsurl = receivedAudio.filePathUrl
        audioPlayer = try!
            AVAudioPlayer (contentsOfURL: nsurl)
        audioPlayer.enableRate = true
        audioPlayer.prepareToPlay( )
        
        //for chipmunk
        audioFile=try! AVAudioFile(forReading: receivedAudio.filePathUrl)
        audioEngine = AVAudioEngine()
        
        //speakers
        try! audioSession.overrideOutputAudioPort(AVAudioSessionPortOverride.Speaker)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playSlowly(sender: UIButton) {
        playAudio(0.5)
    }
    
    @IBAction func playFast() {
        playAudio(1.7)
    }
    
    func playAudio(rate: Float32){
        audioPlayer.stop()
        audioPlayer.currentTime = 0.0
        audioPlayer.rate = rate
        audioPlayer.play()
    }

    @IBAction func playChipmunk(sender: UIButton) {
        playAudioWithVariablePithc(1000)
    }
    
    @IBAction func playDarthVaderAudio(sender: UIButton) {
        playAudioWithVariablePithc(-1000)
    }
    
    func playAudioWithVariablePithc(pitch: Float){
        //stop
        audioPlayer.stop()
        audioEngine.stop()
        audioEngine.reset()
        
        //setup
        let audioPlayerNode = AVAudioPlayerNode()
        audioEngine.attachNode(audioPlayerNode)
        
        let changePitchEffect = AVAudioUnitTimePitch()
        changePitchEffect.pitch = pitch
        audioEngine.attachNode(changePitchEffect)
        
        audioEngine.connect(audioPlayerNode, to:changePitchEffect, format:nil)
        audioEngine.connect(changePitchEffect, to: audioEngine.outputNode, format:nil)
        
        //loadFile
        audioPlayerNode.scheduleFile(audioFile, atTime: nil, completionHandler: nil)
        try! audioEngine.start()
        
        //play
        audioPlayerNode.play()
    }
    
    @IBAction func stop() {
        audioPlayer.stop()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
