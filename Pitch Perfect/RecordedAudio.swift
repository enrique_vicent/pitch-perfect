//
//  RecordedAudio.swift
//  Pitch Perfect
//
//  Created by Enrique Vicent on 21/2/16.
//  Copyright © 2016 Enrique Vicent. All rights reserved.
//

import Foundation

class RecordedAudio {
    var filePathUrl: NSURL!
    var title: String!
}